export interface IHero {
  id: number;
  name: string;
  age: number;
  power: string;
  quote: string;
  photo: string;
}
