import { Component, OnInit } from '@angular/core';
import {HeroService} from './hero.service';
import {IHero} from './hero';

@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.css']
})
export class HeroListComponent implements OnInit {
  pageTitle = 'Tout les héros';
  hero: IHero[];
  errorMessage: string;
  imageHeight = 175;
  imageWidth = 150;
  private _listFilter: string;
  filteredHero: IHero[];
  constructor(private _heroService: HeroService) {}

  get listFilter(): string {
    return this._listFilter;
  }

  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredHero = this._listFilter ? this.performFilter(this.listFilter) : this.hero;
  }

  ngOnInit() {
    this._heroService.getHero()
      .subscribe(
        hero => {
          this.hero = hero;
          this.filteredHero = this.hero;
        },
        error => this.errorMessage = <any>error
      );
  }

  performFilter(filterBy: string): IHero[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.hero.filter((hero: IHero) => hero.name.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }
}
