import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {IHero} from './hero';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

@Injectable()
export class HeroService {
  private _heroUrl = './api/hero/hero.json';
  constructor(private _http: HttpClient) {}

  getHero(): Observable<IHero[]> {
    return this._http.get<IHero[]>(this._heroUrl)
      .do(data => {
        data.forEach(h => {h.photo = '../images/' + h.photo; });
        /*console.log('All:' +  JSON.stringify(data));*/
      })
      .catch(this.handleError);
  }
  private handleError(err: HttpErrorResponse) {
    console.log(err.message);
    return Observable.throw(err.message);
  }
}
