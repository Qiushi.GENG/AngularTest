import { Component, OnInit } from '@angular/core';
import {IHero} from './hero';
import {HeroService} from './hero.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-hero-details',
  templateUrl: './hero-details.component.html',
  styleUrls: ['./hero-details.component.css']
})
export class HeroDetailsComponent implements OnInit {
  pageTitle = 'Un héro en détail';
  hero: IHero;
  errorMessage: string;
  constructor(private _heroService: HeroService, private _route: ActivatedRoute) {}

  ngOnInit() {
    let id = +this._route.snapshot.paramMap.get('id');
    this._heroService.getHero()
      .subscribe(
        hero => {
          this.hero = hero.find(h => h.id === id);
        },
        error => this.errorMessage = <any>error
      );
  }

}
