import { TestBed, inject } from '@angular/core/testing';

import { HeroGuardService } from './hero-guard.service';

describe('HeroGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HeroGuardService]
    });
  });

  it('should be created', inject([HeroGuardService], (service: HeroGuardService) => {
    expect(service).toBeTruthy();
  }));
});
