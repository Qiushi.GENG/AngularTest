import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { HeroService } from './hero/hero.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { HeroListComponent } from './hero/hero-list.component';
import { HeroDetailsComponent } from './hero/hero-details.component';
import { HeroGuardService } from './hero/hero-guard.service';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeroListComponent,
    HeroDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [HeroService, HeroGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
