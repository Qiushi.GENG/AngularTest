import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {HeroListComponent} from './hero/hero-list.component';
import {HeroGuardService} from './hero/hero-guard.service';
import {HeroDetailsComponent} from './hero/hero-details.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {path: 'hero/:id', canActivate: [HeroGuardService] , component: HeroDetailsComponent},
      {path: 'search', component: HeroListComponent},
      {path: '', component: HomeComponent},
      {path: '**', redirectTo: '' , pathMatch: 'full'},
    ]),
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
