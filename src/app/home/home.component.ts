import { Component, OnInit } from '@angular/core';
import {IHero} from '../hero/hero';
import {HeroService} from '../hero/hero.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  pageTitle = 'Bienvenue à la base des Héros !';
  nbHeroToShow = 4;
  hero: IHero[];
  errorMessage: string;
  imageHeight = 175;
  imageWidth = 150;
  constructor(private _heroService: HeroService) {}

  ngOnInit() {
    this._heroService.getHero()
      .subscribe(
        hero => {
          this.hero = hero.slice(0, this.nbHeroToShow);
        },
        error => this.errorMessage = <any>error
      );
  }

}
